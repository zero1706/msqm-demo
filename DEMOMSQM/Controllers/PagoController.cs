﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Web;
using System.Web.Mvc;
using DEMOMSQM.Models;
using DEMOMSQM.Models.ViewModels;
namespace DEMOMSQM.Controllers
{
    public class PagoController : Controller
    {
        // GET: Pago
        public ActionResult Index()
        {
            List<ListTableViewModel> lst;
            using (DEMOPAGOEntities3 db = new DEMOPAGOEntities3())
            {
                lst = (from d in db.VENTAS
                       select new ListTableViewModel
                       {
                           Id = d.id,
                           Usuario = d.Usuario,
                           Fecha = (DateTime)d.Fecha,
                           Beneficiario = d.Beneficiario,
                           Producto = d.Producto,
                           Precio = d.Precio,
                           Email = d.Email,
                           Direccion = d.Direccion



                       }).ToList();

            }
            return View(lst);
        }
        public ActionResult RealizarPago()
        {
            return View();
        }


        public struct Payment
        {
            public string Payor, Payee;
            public int Amount;
            public string DueDate;
        }

        [HttpPost]
        public ActionResult RealizarPago(TablaViewModel model)
        {
            try {
                if (ModelState.IsValid)
                {

                    
                        var oTabla = new VENTAS();
                        Random r = new Random();

                        model.Id = r.Next(10, 900);
                        oTabla.id = model.Id;
                        oTabla.Usuario = model.Usuario;
                        oTabla.Fecha = model.Fecha;
                        oTabla.Beneficiario = model.Beneficiario;
                        oTabla.Producto = model.Producto;
                        oTabla.Precio = model.Precio;
                        oTabla.Email = model.Email;
                        oTabla.Direccion = model.Direccion;




                       
                        using (MessageQueue Testing = new MessageQueue())
                        {
                            Testing.Path = @".\private$\Testing";
                            if (!MessageQueue.Exists(Testing.Path))
                            {
                                MessageQueue.Create(Testing.Path);
                            }




                            System.Messaging.Message mymessage = new System.Messaging.Message();

                            //Payment myPayment;
                            //myPayment.Payor = "aeae";
                            //myPayment.Payee = "aea";
                            //myPayment.Amount = 12;
                            //myPayment.DueDate = "aeae";

                            mymessage.Body ="Pedido n°: "+ oTabla.id + "\n\r" +"Comprador: "+oTabla.Usuario+ "\n\r"+"Fecha: " + oTabla.Fecha + "\n\r"+"Producto: "+ oTabla.Producto + "\n\r" + "Email: " + oTabla.Email + "\n\r" + "Direccion: " + oTabla.Direccion;


                            Testing.Send(mymessage);
                        }

                    

                    return Redirect("/");
                }

            }
             catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return View(model);

            
           
        }
        
    }
}