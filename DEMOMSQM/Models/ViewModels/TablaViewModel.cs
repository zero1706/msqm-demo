﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DEMOMSQM.Models.ViewModels
{
    public class TablaViewModel
    {
        public int Id { get; set; }
       
        [Display(Name = "Usuario")]
        public string Usuario { get; set; }

        [Display(Name = "Beneficiario")]
        public string Beneficiario { get; set; }
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }
        [Display(Name = "Producto")]
        public string Producto { get; set; }
        [Display(Name = "Precio")]
        public string Precio { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }
    }


}