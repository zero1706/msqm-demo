﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DEMOMSQM.Models.ViewModels
{
    public class ListTableViewModel
    {
        public int Id { get; set; }
        public string Usuario { get; set; }

        public string Beneficiario { get; set; }
        public DateTime Fecha { get; set; }

        public string Producto { get; set; }
        public string Precio { get; set; }

        public string Email { get; set;  }
        public string Direccion { get; set;  }
    }
}